package com.example.flutterLogin.controllers;

import java.util.Objects;

import com.example.flutterLogin.config.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.flutterLogin.config.JwtTokenUtil;
import com.example.flutterLogin.models.JwtRequest;
import com.example.flutterLogin.models.JwtResponse;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
//
	@Autowired
	private WebSecurityConfig webSecurityConfig;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
			throws Exception {

		System.out.println("----------------Authen User JwtUserDetailsService----------------");
//		System.out.println(authenticationRequest.getUsername());
//		System.out.println(authenticationRequest.getPassword());
//		System.out.println(webSecurityConfig.passwordEncoder().matches(authenticationRequest.getPassword(), "$2a$10$U.b.xZXQOnZwOgXRO0yZ4eJSDuIHYuRE.4.1R1vJN6kcZ4rmPLTFO"));


		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = jwtInMemoryUserDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		System.out.println("----------------Gen Token JwtTokenUtil----------------");
		final String token = jwtTokenUtil.generateToken(userDetails);

		System.out.println("----------------Got Token!----------------");

		return ResponseEntity.ok(new JwtResponse(token));
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			System.out.println(e);
			throw new Exception("USER_DISABLED", e);

		} catch (BadCredentialsException e) {
			System.out.println(e);
			throw new Exception("INVALID_CREDENTIALS", e);

		}
	}
}
