package com.example.flutterLogin.controllers;


import com.example.flutterLogin.config.JwtTokenUtil;
import com.example.flutterLogin.models.MyResponse;
import com.example.flutterLogin.repositories.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@ControllerAdvice
@RequestMapping("/api/v1")

public class UserController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AppUserRepository appUserRepository;

    @GetMapping("/getUser")
    public @ResponseBody ResponseEntity<Object> getUser(){
        MyResponse mrp = new MyResponse();
        mrp.setStatus("success");
        mrp.setData(appUserRepository.findAll());
        return new ResponseEntity<>(mrp, HttpStatus.OK);
    }

    @RequestMapping({ "/tokenDetail" })
    public @ResponseBody ResponseEntity<Object> tokenDetail(@RequestHeader(value="Authorization")String token) {
        String subToken = token.substring(7);
        MyResponse mrp = new MyResponse();
        Map<String, ?> m = Map.of(
                "name", jwtTokenUtil.getUsernameFromToken(subToken),
                "issuedDate", jwtTokenUtil.getIssuedAtDateFromToken(subToken),
                "expireDate", jwtTokenUtil.getExpirationDateFromToken(subToken)

        );
        mrp.setStatus("success");
        mrp.setData(m);
        mrp.setMessage("");
        return new ResponseEntity<>(mrp, HttpStatus.OK);
    }

    @RequestMapping({ "/test" })
    public String test() {

        return "hahahahahahahahahahahahahahaha";
    }








//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<Object> notFoundException() {
//        MyResponse mrp = new MyResponse();
//        mrp.setStatus("GGGGGGGG");
//        return new ResponseEntity<>(mrp, HttpStatus.UNAUTHORIZED);
//    }

}


