package com.example.flutterLogin.repositories;

import com.example.flutterLogin.models.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface AppUserRepository extends JpaRepository<AppUser, Integer> {
    public List<AppUser> findByUserName(String name);

}
