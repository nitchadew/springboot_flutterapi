package com.example.flutterLogin.service;

import java.util.ArrayList;

import com.example.flutterLogin.models.AppUser;
import com.example.flutterLogin.repositories.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private AppUserRepository appUserRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (!appUserRepository.findByUserName(username).isEmpty()) {
//			return new User(username, "$2a$10$64SrMkMZz/s0PvZ0eNn1hOQVpBu0kbNQZYOjWe3agnLKwrbzgCC3y",
//					new ArrayList<>());
			return new User(username, appUserRepository.findByUserName(username).get(0).getUserPassword(),
					new ArrayList<>());
		} else {
			System.out.println("User not found with username: " + username);
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

}